import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

export default class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      editing: false,
    };
  }

  tabIcons = [];

  static propTypes = {
    goToPage: React.PropTypes.func,
    activeTab: React.PropTypes.number,
    tabs: React.PropTypes.array,
  };

  goToPageInput = (n) => {
    this.props.goToPage(n);
    this.setState({ editing: true });
  }

  goToPage = (n) => {
    this.props.goToPage(n);
    this.refs.input.blur();
  }

  render() {
    return (
      <View style={[styles.tabs, this.props.style ]}>
        <View style={styles.searchbar}>
          <TextInput
            style={styles.input}
            blurOnSubmit={true}
            placeholder="Enter location"
            onChangeText={(text) => this.setState({text})}
            onFocus={() => this.goToPageInput(0)}
            onBlur={() => this.setState({editing: false})}
            ref="input"
            returnKeyType="done"
            value={this.state.text}/>
          <Icon style={styles.inputIcon} name="search" size={20}
                color={this.state.editing ? '#176992' : 'rgb(204,204,204)'}/>
        </View>
        <View style={styles.actions}>
          <TouchableOpacity onPress={() => this.goToPage(1)} style={styles.tab}>
            <Icon name="location-arrow" size={20} color={this.props.activeTab === 1 ? '#176992' : 'rgb(204,204,204)'}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.goToPage(2)} style={styles.tab}>
            <Icon name="bullhorn" size={20} color={this.props.activeTab === 2 ? '#176992' : 'rgb(204,204,204)'}/>
            <View style={styles.badge}>
              <Text style={styles.badgeText}>3</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.goToPage(3)} style={styles.tab}>
            <Icon name="facebook-official" size={20}
                  color={this.props.activeTab === 3 ? 'rgb(59,89,152)' : 'rgb(204,204,204)'}/>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  input: {
    backgroundColor: '#fff',
    height: 40,
    paddingLeft: 35,
    fontSize: 13,
    color: '#176992',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputIcon: {
    position: 'absolute',
    top: 10,
    left: 10,
    backgroundColor: 'transparent'
  },
  searchbar: {
    backgroundColor: '#eee',
    flex: 1
  },
  actions: {
    width: 140,
    flexDirection: 'row',
  },
  tab: {
    flex: 1,
    paddingTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
  },
  tabs: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.05)',
    marginTop: 20,
    borderTopWidth: 0
  },
  badge: {
    position: 'absolute',
    top: 1,
    right: 1,
    width: 15,
    height: 15,
    borderRadius: 15,
    backgroundColor: '#FF0000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeText: {
    color: '#fff',
    fontSize: 10
  }
});