import React, { Component } from 'react';
import Svg, {
  Circle,
  Ellipse,
  G,
  LinearGradient,
  RadialGradient,
  Line,
  Path,
  Polygon,
  Polyline,
  Rect,
  Symbol,
  Text,
  Use,
  Defs,
  Stop
} from 'react-native-svg';

export default class Chart extends Component {

  _generateData(val, amount, width, height) {
    const proportion = height / val;
    const times = width / amount;
    let result = '';
    for (var i = 0; i < amount; i++) {
      result += `${i * times},${(Math.floor(Math.random() * val * proportion) + 1)} `;
    }
    return result;
  }

  render() {
    return (
      <Svg height="50"
           width="400">
        <Polyline
          fill="none"
          stroke="#fafafa"
          strokeWidth="2"
          points={this._generateData(40, 40, 400, 50)}
        />
      </Svg>
    );
  }
}
