import React, { Component } from 'react';

import {
  View,
  Image,
  Text,
  StyleSheet
} from 'react-native';

export default class Profile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      avatar: '../images/elite-dating-background2.jpg'
    }
  }

  _renderAvatar() {
    return (
      <Image style={styles} uri={this.state.avatar}/>
    )
  }

  render() {
    return (
      <View style={styles.root}>
        {this._renderAvatar()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  }
});