import React, { Component } from 'react';
import {
  StyleSheet,
} from 'react-native';
import { Scene, Router } from 'react-native-router-flux';

import MainPage from './pages/main-page';
import SpotModal from './pages/spot-modal';
import LookPage from './pages/look-page';

export default class AppRouter extends Component {
  render() {
    return <Router>
      <Scene key="root" navigationBarStyle={styles.nav}>
        <Scene key="main" component={MainPage} hideNavBar={true}/>
        <Scene key="spot" component={SpotModal} hideNavBar={false}/>
        <Scene key="look" component={LookPage} hideNavBar={true}
               navigationCardStyle={styles.look} type="push"
               direction='vertical'/>
      </Scene>
    </Router>
  }
}
const styles = StyleSheet.create({
  nav: {
    backgroundColor: '#fff',
    borderBottomColor: 'rgba(0,0,0,0.05)'
  },
  look: {
    margin: 50,
    backgroundColor: 'transparent',
    shadowColor: 'transparent',
  }
});