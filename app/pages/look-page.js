import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome';

export default class LookPage extends Component {
  render() {
    return (
      <View style={styles.root}>
        <View>
          <TouchableOpacity onPress={Actions.pop}>
            <Icon name="times" size={30} color="#eee" />
          </TouchableOpacity>
        </View>
        <View style={styles.spot}>
          <Text>Look</Text>
          <Text>{this.props.marker.latlng.latitude}</Text>
          <Text>{this.props.marker.latlng.longitude}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    marginTop: 60,
    backgroundColor: "transparent",
  },
  spot: {
    padding: 5
  },
});