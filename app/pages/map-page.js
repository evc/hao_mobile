import React, { Component } from 'react';

import {
  // MapView,
  Image,
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';

import { Actions } from 'react-native-router-flux'
import MapView from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class MapPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      near: 6,
      location: {
        latitude: 43.2612413,
        longitude: 76.96144829999999,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      markers: [
        {
          latlng: {
            latitude: 43.2842413,
            longitude: 76.9144829999999
          },
          image: require('./girls/1.jpg')
        },
        {
          latlng: {
            latitude: 43.2722413,
            longitude: 76.92244829999999
          },
          image: require('./girls/2.jpg')
        },
        {
          latlng: {
            latitude: 43.2642413,
            longitude: 76.96344829999999
          },
          image: require('./girls/3.png')
        },
        {
          latlng: {
            latitude: 43.2812413,
            longitude: 76.98944829999999
          },
          image: require('./girls/4.jpg')
        },
        {
          latlng: {
            latitude: 43.2552413,
            longitude: 76.95144829999999
          },
          image: require('./girls/5.jpg')
        },
        {
          latlng: {
            latitude: 43.2962413,
            longitude: 76.93644829999999
          },
          image: require('./girls/6.jpg')
        }]
    }
  }

  _markerClick(marker) {
    Actions.look({ marker });
  }

  render() {
    return (
      <View>

        <MapView style={styles.map}
                 initialRegion={this.state.location}>
          <MapView.Marker key={'myposition'}
                          coordinate={this.state.location}
                          title={'My position'}
                          description={'Reading and comsuming' }>
            <View style={styles.myMarker}>
              <Icon name="map-marker" color="#176992" size={40} style={styles.myMarkerImage}/>
            </View>
          </MapView.Marker>

          {this.state.markers.map((marker, index) => (
            <MapView.Marker key={index}
                            coordinate={marker.latlng}
                            title={'Angelina Joe Li'}
                            description={'Relaxing and drinking coffee' }>
              <TouchableOpacity style={styles.marker} onPress={() => this._markerClick(marker)}>
                <Image style={styles.markerImage} source={marker.image}/>
              </TouchableOpacity>
            </MapView.Marker>
          ))}
        </MapView>
        <View style={styles.infobar}>
          <Text style={styles.infobarText}>
            {this.state.near} girls nearby online
          </Text>
          <Text style={styles.infobarTextSub}>Come close enough and start chatting</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    height: Dimensions.get("window").height
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height - 60
  },
  infobar: {
    position: 'absolute',
    bottom: 0,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  infobarText: {
    padding: 5,
    backgroundColor: 'rgba(255,255,255,.7)'
  },
  infobarTextSub: {
    color: 'grey',
    fontSize: 12,
    marginLeft: 30,
    backgroundColor: 'transparent'
  },
  marker: {},
  markerImage: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 3,
    borderColor: '#fff'
  },
  myMarker: {
    backgroundColor: 'rgba(0,0,0,.1)',
    width: 100,
    height: 100,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  myMarkerImage: {
    width: 40,
    height: 40,
    marginLeft: 20,
    backgroundColor: 'transparent',
  }
});