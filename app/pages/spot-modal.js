import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text
} from 'react-native';

export default class SpotModal extends Component {
  render() {
    return (
      <View style={styles.root}>
        <View style={styles.spot}>
          <Text>Spot</Text>
          <Text>{this.props.rowData.title}</Text>
          <Text>{this.props.rowData.description}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingTop: 60,
  },
  spot: {
    padding: 5
  }
});