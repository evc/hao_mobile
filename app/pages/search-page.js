import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  RefreshControl,
  ListView,
  TouchableOpacity
} from 'react-native';

import Chart from  '../components/chart';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux'

export default class SearchPage extends Component {

  constructor(props) {
    super(props);


    const data = [
      {
        title: 'Red alert, galaxy!',
        data: [1, 5, 3, 34, 5, 23, 6],
        description: 'Instead of varnishing mild buttermilk with pork butt, use a dozen peaces vinegar and seven pounds vegemite casserole.',
        total: 56,
        percent: 45,
      },
      {
        title: 'Heu.',
        data: [36, 25, 13, 12, 11, 13, 61],
        description: 'Try toasting broccoli pie blended with anchovy essence.',
        total: 54,
        percent: 45,
      },
      {
        title: 'Cum',
        data: [11, 52, 33, 34, 55, 23, 76],
        description: 'All children like drained bagels in BBQ sauce and butter.',
        total: 78,
        percent: 33,
      },
      {
        title: 'Sunt',
        data: [1, 5, 3, 34, 5, 23, 6],
        description: 'Roast crushed lobsters in a soup pot with vinegar for about an hour to perfect their sourness.',
        total: 78,
        percent: 34,
      },
      {
        title: 'Accola clemens',
        data: [0, 0, 0, 1, 2, 3, 1],
        description: 'When crushing sweet sausages, be sure they are room temperature.',
        total: 67,
        percent: 34,
      },
      {
        title: 'Coordinataes studere',
        data: [5, 5, 3, 4, 51, 53, 36],
        description: 'After chopping the blood oranges, brush meatballs, onion and BBQ sauce with it in a pan.',
        total: 23,
        percent: 3,
      },
      {
        title: 'Grandis',
        data: [1, 1, 3, 24, 5, 23, 3],
        description: 'When mashing muddy bok choys, be sure they are room temperature.',
        total: 12,
        percent: 50,
      }
    ];

    var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows(data),
      refreshing: false
    }
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 200)
  };

  _renderRow(rowData) {
    return (
      <TouchableOpacity onPress={() => Actions.spot({rowData})}>
        <Chart style={styles.chart}/>
        <View style={styles.row}>
          <View style={styles.rowInfo}>
            <Text style={styles.rowTitle}>
              {rowData.title}
            </Text>
            <Text style={styles.rowDescription} numberOfLines={1}>
              {rowData.description}
            </Text>
          </View>
          <View style={styles.rowStats}>
            <Text style={styles.rowStatsText}>
              {rowData.total}
              <Icon name="venus-mars" size={14} color='#176992'/>
            </Text>
            <Text style={styles.rowStatsText}>
              {rowData.percent}%
              <Icon name="venus" size={14} color='#176992'/>
            </Text>
          </View>
        </View>
      </TouchableOpacity>

    )
  }

  render() {
    return (
      <View style={styles.root}>
        <ListView
          style={styles.list}
          dataSource={this.state.dataSource}
          renderRow={(rowData) => this._renderRow(rowData)}
          refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/>}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  list: {
    padding: 5
  },
  chart: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 40,
    // width: Dimensions.get('window').width
  },
  row: {
    position: 'absolute',
    backgroundColor: 'transparent',
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
    flex: 1,
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  rowTitle: {
    fontSize: 16,
    color: '#176992',
    paddingLeft: 5
  },
  rowDescription: {
    flexDirection: 'column',
    fontSize: 12,
    color: '#1ABAB7',
    paddingLeft: 5
  },
  rowInfo: {
    flex: 1,
    justifyContent: 'center',
    height: 40,
  },
  rowStats: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowStatsText: {
    color: 'grey',
    fontSize: 14,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  image: {
    width: 40,
    height: 40
  },
});