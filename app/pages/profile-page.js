import React, { Component } from 'react';

import {
  View,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  TextInput,
  TouchableHighlight
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';


export default class ProfilePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      avatar: './elite-dating-background2.jpg',
      background: './elite-dating-background2.jpg',
      status: 'Combine truffels, shrimps and chili. soak with cored brine and serve sliced with bok choy. Enjoy!'
    }
  }

  _renderAvatar() {
    return (
      <Image style={styles.avatar} source={require('./avatar.jpg')}/>
    )
  }

  _renderBackground() {
    return (
      <Image style={styles.background} source={require('./background.jpg')}/>
    )
  }

  _renderActions() {
    return (
      <View style={styles.actions}>
        <TouchableHighlight style={styles.buttonWrapper}>
          <View style={styles.button}>
            <Icon name="pencil-square-o" size={14} color='#000' style={{paddingRight: 4}}/>
            <Text style={styles.actionText}>
              New post
            </Text>
          </View>
        </TouchableHighlight>
        <TouchableHighlight style={styles.buttonWrapper}>
          <View style={styles.button}>
            <Icon name="sign-out" size={14} color='#000' style={{paddingRight: 4}}/>
            <Text style={styles.actionText}>
              Logout
            </Text>
          </View>
        </TouchableHighlight>
      </View>
    )
  }

  _renderStatus = () => {
    return (
      <View style={styles.status}>
        <TextInput style={styles.statusText} value={this.state.status}
                   onChange={(val) => this.setState({status: val})}/>
      </View>
    )
  };

  render() {
    return (
      <ScrollView style={styles.root}>
        {this._renderBackground()}
        {this._renderAvatar()}
        {this._renderActions()}
        {this._renderStatus()}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    height: 200
  },
  avatar: {
    width: 100,
    height: 100,
    marginTop: -50,
    marginLeft: 25,
    borderWidth: 4,
    borderColor: '#fff',
    borderRadius: 6,
    marginBottom: 40,
  },
  background: {
    width: Dimensions.get('window').width,
    height: 170
  },
  status: {
    padding: 20,
  },
  statusText: {
    color: '#176992'
  },
  actions: {
    marginTop: -80,
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    width: Dimensions.get('window').width / 2,
    marginLeft: Dimensions.get('window').width / 2.8
  },
  button: {
    alignItems: 'center',
    flexDirection: 'row',
    padding: 5,
  },
  buttonWrapper: {
    backgroundColor: 'rgb(234,234,234)',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'rgb(214,214,214)',
  },
  actionText: {
    fontSize: 13,
  },
});