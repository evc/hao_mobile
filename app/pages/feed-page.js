import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  RefreshControl,
  ListView
} from 'react-native';

export default class FeedPage extends Component {

  constructor(props) {
    super(props);

    const data = [
      {
        title: 'Heu.',
        description: 'Try',
        image: require('./girls/1.jpg')
      },
      {
        title: 'Cum',
        description: 'All children like drained bagels in BBQ sauce and butter.',
        image: require('./girls/2.jpg')
      },
      {
        title: 'Sunt',
        description: 'Roast crushed lobsters in a soup pot with vinegar for about an hour to perfect their sourness.',
        image: require('./girls/3.png')
      },
      {
        title: 'Accola clemens',
        description: 'When crushing sweet sausages, be sure they are room temperature.',
        image: require('./girls/4.jpg')
      },
      {
        title: 'Coordinataes studere',
        description: 'After chopping the blood oranges, brush meatballs, onion and BBQ sauce with it in a pan.',
        image: require('./girls/5.jpg')
      },
      {
        title: 'Grandis',
        description: 'When mashing muddy bok choys, be sure they are room temperature.',
        image: require('./girls/6.jpg')
      }
    ];

    var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows(data),
      refreshing: false
    }
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 200)
  };

  _renderRow(rowData) {
    return (
      <View style={styles.rowWrapper}>
        <View style={styles.row}>
          <Image style={styles.image} source={rowData.image}/>
          <View style={styles.rowInfo}>
            <Text style={styles.rowTitle}>
              {rowData.title}
            </Text>
            <Text style={styles.rowDescription} numberOfLines={1}>
              {rowData.description}
            </Text>
          </View>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.root}>
        <ListView
          style={styles.list}
          dataSource={this.state.dataSource}
          renderRow={(rowData) => this._renderRow(rowData)}
          refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/>}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#efefef',
  },
  list: {
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,

  },
  rowWrapper: {
    borderRadius: 10,
    marginBottom: 10,
    overflow: 'hidden',
    shadowColor: "#000000",
    shadowOpacity: 0.5,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 1
    },
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 12,
    paddingRight: 10,
  },
  rowTitle: {
    fontSize: 14,
    color: '#aaa',
    paddingLeft: 10,
    paddingTop: 6,
    paddingBottom: 5,
  },
  rowDescription: {
    flexDirection: 'column',
    fontSize: 14,
    color: '#176992',
    paddingLeft: 10,
    position: 'absolute',
    bottom: 5,
  },
  rowInfo: {
    flex: 1,
    height: 60,
  },
  image: {
    width: 40,
    height: 60,
    backgroundColor: 'red'
  },
});