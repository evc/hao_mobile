import React, { Component } from 'react';
import {
  StyleSheet,
} from 'react-native';

const ScrollableTabView = require('react-native-scrollable-tab-view');

import Navbar from '../components/navbar';
import MapPage from '../pages/map-page';
import ProfilePage from '../pages/profile-page';
import SearchPage from '../pages/search-page'
import FeedPage from '../pages/feed-page'

export default class MainPage extends Component {
  render() {
    return (
      <ScrollableTabView style={styles.root}
                         initialPage={3}
                         renderTabBar={() => <Navbar />}>
        <SearchPage tabLabel="Search"/>
        <MapPage tabLabel="Near"/>
        <FeedPage tabLabel="Feed"/>
        <ProfilePage tabLabel="Feed"/>
      </ScrollableTabView>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});