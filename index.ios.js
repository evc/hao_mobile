import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';

import AppRouter from './app/router';

class mobile extends Component {
  render() {
    return (
      <AppRouter />
    )
  }
}

AppRegistry.registerComponent('mobile', () => mobile);
